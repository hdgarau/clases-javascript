/**************************************************************************/
/************** TANDA DE EJERCICIOS - ALUMNOS *****************************/
/**************************************************************************/

//1.1 Definir dos Arrays de objetos Alumnos... (5 elementos C/U)
// - Cada alumno tiene:
//		* id
//		* nom_ap
//		* genero (M, F, X)
//		* edad

let alumnos1 = [
		{
        "id": "A1",
        "nom_ap": "Mariana Espósito",
        "genero": "F",
        "edad": 20
    },

    {
        "id": "A2",
        "nom_ap": "Adrián Martínez",
        "genero": "M",
        "edad": 36
    },

    {
        "id": "A3",
        "nom_ap": "Gabriel Julio Fernández",
        "genero": "M",
        "edad": 54
    },

    {
        "id": "A4",
        "nom_ap": "Ana Casanova",
        "genero": "F",
        "edad": 91
    },

    {
        "id": "A5",
        "nom_ap": "Diego Caccia",
        "genero": "M",
        "edad": 53
    },
];

let alumnos2 = [
		{
        "id": "B1",
        "nom_ap": "Jennifer Annastassakis",
        "genero": "F",
        "edad": 25
    },

		{
				"id": "B2",
				"nom_ap": "Clotilde Acosta",
				"genero": "F",
				"edad": 67
		},

    {
        "id": "B3",
        "nom_ap": "Margarita Cansino",
        "genero": "F",
        "edad": 33
    },

    {
        "id": "B4",
        "nom_ap": "Édgar Gómez",
        "genero": "M",
        "edad": 47
    },

    {
        "id": "B5",
        "nom_ap": "Álvaro Osorio",
        "genero": "M",
        "edad": 38
    },

    {
        "id": "A5",
        "nom_ap": "Clotilde Acosta",
        "genero": "F",
        "edad": 67
    },
];

let alumnos3 = [
	new Alumno('A5', "Clotilde Acosta", "F", 67),
	new Alumno('B4', "Édgar Gómez", "M", 47),
];


//1.2 Hacer una funcion que recibe un array de alumnos y devuelve un array solo con las mujeres (genero F)

function soloMujeres(alumnos){
var esMujer = false;
var mujeres = [];

	for(let alumno of alumnos){

		if(alumno.genero === "F"){
			esMujer = true;
			mujeres.push(alumno);
		}
	}

return(mujeres)
};
