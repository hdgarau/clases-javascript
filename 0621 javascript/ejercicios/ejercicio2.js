//Clase: crea una estructura común hay dos modos de definir clases: funciones y con la plabra reservada "class"

// Ejemplo con función (va la primera con mayúscula)
function Alumno(id, nom_ap, genero, edad){
  this.id = id;
  this.nom_ap = nom_ap;
  this.genero = genero;
  this.edad = edad;

  this.setGenero = function (gen){
    this.genero = gen;
  }

  this.getDato = function (dato){
    console.log(this[dato]);
  }
};

// Ejemplo con class (revisar)
class Alumno2 {
  constructor(id, nom_ap, genero, edad) {
    this.id = id;
    this.nom_ap = nom_ap;
    this.genero = genero;
    this.edad = edad;
  }
    setGenero = function (gen){
      this.genero = gen;
    }

    getDato = function (dato){
      console.log(this[dato]);
    }
};

function listAlumn(alumnos){
  this.alumnos = [];
  this.addAlumnos = function(alumnos){

    if(!Array.isArray(alumnos)){
      alert('No es array');
      return false;
    }

    for (let alumno of alumnos){
      this.addAlumno(alumno)
    }
  }

  this.addAlumno = function(alumno){
    //validar que el objeto tenga todas las propiedades
    if(typeof alumno != 'object'){
      alert('Objeto no válido');
      return false;
    }

    if(!alumno.hasOwnProperty('id')){
      alert('No tiene ID');
      return false;
    }

    this.alumnos.push(new Alumno(alumno.id, alumno.nom_ap, alumno.genero, alumno.edad));
    return true;
  }

this.addAlumnos(alumnos);
};
