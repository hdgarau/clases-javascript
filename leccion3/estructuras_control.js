//Sintaxis
//varible = valor ; condicion (mientras -while); accion
//value start; condition(while); do
for (let desde = 0; desde < 5; desde++)
{
	console.log(desde);
}
	
let arr = [
	1,
	'a',
	['pp',1],
	{}, 
	()=>''
];

//estos dos for son lo mismo
let index ;

for ( index = 0; index < arr.length ; index++)
{
	console.log(index, arr[index]);
}

for ( index in arr) //maera simplificada de recorrer u array por indice
{
	console.log(index, arr[index]);
}

///---------------
for ( let val of arr) //maera simplificada de recorrer u array por valor
{
	console.log(val);
}

//--------
///si se cumple la condicion entro y repito hasta que  no se cumpla
index = 0;
do//la primera vez no pregunta
{
	index ++;
	console.log(index);
}while(index != 5)

index = 0;
//la primera vez pregunta
while(index != 5)
{
	index ++;
	console.log('entre')
}


//el ! al principio significa not ej:
if(!false)//entra siempre
{
	console.log('Entre');
} 

//expresiones binarias (if simplificados)
//[condicion] ? [lo que RETORNA se comple] :  [lo que RETORNA si no se cumple]
// Útil para usar en asignaciones.
//ej 1
let color = 'rojo';
let pasar = color == 'verde' ? 'si' : 'false';
//ej2
console.log( [1,2].length > 2 ? 'tengo 3 o mas elementos' : 'tengo menos de 2');

//para simplificar cuando tenes que hacer algo segun un valor específico
let val = 1;
switch(typeof val)
{
	case 'number':
		console.log(1+val);
		break;
	case 'string':	
		console.log('hola ' +val);
		break;
	case 'object':
		console.log(val.join(' '));
		break;
		
}

let arr = [1,2,3,4];
arr.map(function(el){
	return el + 1;
});
