//Ejercicio 1
//se tiene el siguiente codigo

function saludar ( nombre )
{
	//.....
	return  'Hola ' + nombre ;
}

alert ( saludar ( 'Flavio' ) );

//Porque no anda?

//-----------------------------------------------------------------------------------------------

//Ejercicio 2 
//Variables
const Algo = ( algo ) => algo.split();

let objeto = 
{
	'atributo1' : 'attr1',
	'atributo2' : 
	{
		'atributo1': 
		[
			5,
			Algo , 
			'a' , 
			{ 
				'op1' : Algo('HOLA'), 
				'op2' : saludar
			}
		]
	},
	'atributoN' : 'attrN'
};

//Cargo el array
let arrayCosas = [];
arrayCosas.push ( 2 + 2  ); 		//pos 0
arrayCosas.push ( 2 + '2' );		//pos 1
arrayCosas.push ( (a,b) => a + b );	//pos 2
arrayCosas.push ( objeto.atributo2 );			//pos 3



//Que Hace la función Algo?
//Que Retorna cada posicion del array (tipo de dato)?.
//Que retorna arrayCosas[2]( arrayCosas[1], arrayCosas[2] ( arrayCosas[0] , 2 ) ); 
//Como puedo lograr acceder a la funcion Algo que esta dentro del array?

//Nota: Algo podría estar definida de las siguientes maneras:
/*
function Algo(algo)
{
	return algo.split();
}

//o bien
const Algo = (algo) => { return algo.split(); };