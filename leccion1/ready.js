	$(document).ready( function () {
		//TRAIGO EL ELEMENTO HTML
		const fileSelector = document.getElementById('file-selector');
		
		//AGREGO EL EVETO CHANGE
		fileSelector.addEventListener('change', (event) => {
			const fileList = event.target.files; //Le asigno el objeto con los datos del archivo
			readFile( fileList[0] );

		});
	});
	
	function readFile( file ) {
		const reader = new FileReader();
		reader.addEventListener('load', function (event)  {
			const result = event.target.result;
			//console.log(result);
			// Do something with result
			if(file.type == 'application/json')
			{
				///console.log(JSON.parse(result));
				let rFile = new resultFileToElementHTML(JSON.parse(result)[0].data);
				$(':input#file-selector').after(rFile.toTable($('<tr><th>id</th><th>Mensaje</th><th>Hashtag</th></tr>')));				
			}
			else
			{
				return result;
			}
		});
	/*
	  reader.addEventListener('progress', (event) => {
		if (event.loaded && event.total) {
		  const percent = (event.loaded / event.total) * 100;
		  console.log(`Progress: ${Math.round(percent)}`);
		}
	  }); */
		 reader.readAsText(file);
	}
	
	//classes
	class resultFileToElementHTML
	{
		_data = null;
		
		constructor(data) {this._data = data;}
		
		toTable(head) { 
			let t = $('<table>');
			if(typeof head !== 'undefined')
			{
				t.append(head);
			}
			for(let row of this._data)	 
			{
				let tr = $('<tr>');
				console.log(row);
				for(let attr in row)	 
				{
					tr.append($('<td>' + row[attr] + '</td>'));
				}
				t.append(tr);
			}
			return t;
		}
	}